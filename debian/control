Source: console-bridge
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jose Luis Rivero <jrivero@osrfoundation.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-exec (>=0.3),
               cmake,
               libgtest-dev
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/console-bridge
Vcs-Git: https://salsa.debian.org/science-team/console-bridge.git
Homepage: https://github.com/ros/console_bridge

Package: libconsole-bridge1.0
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: console bridge - library
 ROS-independent, pure CMake (i.e. non-catkin and non-rosbuild
 package) that provides logging calls that mirror those found in
 rosconsole, but for applications that are not necessarily using ROS.
 .
 This package contains the dynamic library.

Package: libconsole-bridge-dev
Architecture: any
Section: libdevel
Depends: libconsole-bridge1.0 (= ${binary:Version}),
	 ${misc:Depends}
Multi-Arch: same
Description: console bridge - development files
 ROS-independent, pure CMake (i.e. non-catkin and non-rosbuild
 package) that provides logging calls that mirror those found in
 rosconsole, but for applications that are not necessarily using ROS.
 .
 This package contains the development files (headers, pkg-config and
 CMake files).
